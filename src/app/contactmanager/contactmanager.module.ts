import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';

// interface component to angular material controls
import { MaterialModule } from '../shared/material.module';

import { ContactmanagerComponent } from './contactmanager.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { MainContentComponent } from './components/main-content/main-content.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { UserService } from './services/user.service';
import { NotesComponent } from './comomponents/notes/notes.component';
import { NewContactDialogComponent } from './components/new-contact-dialog/new-contact-dialog.component';



const routes: Routes = [
  // => /contactmanager which loads <side-nav> component
  { path: '', component: ContactmanagerComponent,
  // => side-nav loads <toolbar> and  <app-maincontent>
    children: [
      { path: ':id', component: MainContentComponent},
      { path: '', component: MainContentComponent}
    ]
  },
  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    // application modules
    MaterialModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    UserService
  ],
  declarations: [ ContactmanagerComponent,
                  ToolbarComponent,
                  MainContentComponent,
                  SidenavComponent,
                  NotesComponent,
                  NewContactDialogComponent
                ],
                entryComponents: [
                  NewContactDialogComponent
                ]
})
export class ContactmanagerModule { }

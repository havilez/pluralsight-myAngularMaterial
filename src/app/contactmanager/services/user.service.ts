import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { resolve } from 'q';

import { User } from '../models/user';


@Injectable()
export class UserService {

  private _users: BehaviorSubject<User[]>;

  private dataStore: {
      users: User[];
  };

  constructor(private http: HttpClient) {
    this.dataStore = {users: [] };
    // create a behavior subjec of type User[] and initialize it to empty array
    this._users = new BehaviorSubject<User[]>([]);

  }

  // getter method to retrieve users
  // used by components to subscribe to data
   get users(): Observable<User[]> {
     return this._users.asObservable();
   }

   loadAll() {
       // external data source
      const userUrl = 'https://angular-material-api.azurewebsites.net/users';

       return this.http.get<User[]>(userUrl)
                .subscribe( data => {
                    this.dataStore.users = data;
                    // copy each user in  data array in data store to empty object
                     // publishes data to all listening subjects
                    this._users.next( Object.assign({}, this.dataStore).users);
                }, error => {
                    console.log('Failed to fetch users');
                });
    }

    userById( id: number ) {
        console.log('id in service = ', id );
        return this.dataStore.users.find( x => x.id === id);
    }

    addUser (user: User): Promise<User> {
        return new Promise((resolver, reject ) => {
            user.id = this.dataStore.users.length + 1;
            this.dataStore.users.push(user);
            this._users.next( Object.assign({}, this.dataStore).users);
            resolver(user);
        });
    }

}

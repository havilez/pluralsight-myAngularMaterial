import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { UserService } from '../../services/user.service';
import { User } from '../../models/user';

const SMALL_WIDTH_BREAKPOINT = 720;


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  private mediaMatcher: MediaQueryList = matchMedia(`(max-width: ${SMALL_WIDTH_BREAKPOINT}px)`);

  users: Observable<User[]>;
  isDarkTheme: boolean = false;
  dir: string = 'ltr';


  constructor(zone: NgZone,
              private userService: UserService,
              private router: Router) {

    // check media query list as window is resized
     this.mediaMatcher.addListener(mql => {
        zone.run( () => {
          this.mediaMatcher = mql;
        });
     });

   }

  @ViewChild(MatSidenav) sidenav: MatSidenav;

  ngOnInit() {
    this.users = this.userService.users;

    // retrieve data from external source via async,
    // which is eventually stored internally to service???
    this.userService.loadAll();


    this.router.events.subscribe( () => {
      if ( this.isScreenSmall()) {
        this.sidenav.close();
      }

    });
  }


  toggleDir() {
    this.dir = this.dir === 'ltr' ? 'rtl' : 'ltr';
    this.sidenav.toggle().then(() => this.sidenav.toggle());
  }

   isScreenSmall(): boolean {
    return this.mediaMatcher.matches;
  }

  toggleTheme() {
   this.isDarkTheme = !this.isDarkTheme;

  }


}

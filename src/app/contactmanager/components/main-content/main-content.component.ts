import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {

  user: User;

  constructor(
    private route: ActivatedRoute,
    private service: UserService) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      // convert params strig to number to allow for comparison in service
      let id = Number( params['id'] );
      if (!id) { id = 1; }

      // triggers spinner to show and then disapper?????
      this.user = null;

      this.service.users.subscribe( users  => {

        if ( users.length === 0 ) { return; }

        setTimeout(() => {
          // this.user = users[0];
         this.user = this.service.userById(id);
        console.log('user = ', this.user );
        }, 500);
      });


    });
  }

}
